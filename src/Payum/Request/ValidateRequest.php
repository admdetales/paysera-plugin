<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Request;

use Omni\Sylius\PayseraPlugin\Model\Request;

/**
 * Class ValidateRequest.
 */
final class ValidateRequest
{
    /**
     * @var bool
     */
    private $valid = false;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $orderId;

    /**
     * ValidateRequest constructor.
     *
     * @param Request $request
     * @param string $password
     * @param string $orderId
     */
    public function __construct(Request $request, string $password, string $orderId)
    {
        $this->request = $request;
        $this->password = $password;
        $this->orderId = $orderId;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param bool $valid
     *
     * @return $this
     */
    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }
}
