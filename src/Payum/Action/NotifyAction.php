<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Action;

use Omni\Sylius\PayseraPlugin\Payum\Request\GetRequest;
use Omni\Sylius\PayseraPlugin\Payum\Request\ValidateRequest;
use Omnipay\Paysera\Gateway;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Payum;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\Notify;
use SM\Factory\FactoryInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Payment\PaymentTransitions;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NotifyAction.
 *
 * @property Gateway $api
 */
final class NotifyAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;

    /**
     * @var FactoryInterface
     */
    private $smFactory;

    /**
     * @var Payum
     */
    private $payum;

    /**
     * @var bool
     */
    private $useOrderNumber = false;

    /**
     * NotifyAction constructor.
     *
     * @param FactoryInterface $smFactory
     * @param Payum $payum
     * @param bool $useOrderNumber
     */
    public function __construct(FactoryInterface $smFactory, Payum $payum, bool $useOrderNumber)
    {
        $this->apiClass = Gateway::class;
        $this->smFactory = $smFactory;
        $this->payum = $payum;
        $this->useOrderNumber = $useOrderNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($notify): void
    {
        /** @var Notify $notify */
        $request = new GetRequest();
        $this->gateway->execute($request);
        if (null === $request->result) {
            throw new HttpResponse('', Response::HTTP_BAD_REQUEST);
        }

        /** @var PaymentInterface $payment */
        $payment = $notify->getModel();

        $valid = new ValidateRequest(
            $request->result,
            $this->api->getPassword(),
            $this->useOrderNumber ? $payment->getOrder()->getNumber() : (string)$payment->getOrder()->getId()
        );
        $this->gateway->execute($valid);

        if (false === $valid->isValid()) {
            throw new HttpResponse('', Response::HTTP_BAD_REQUEST);
        }

        $sm = $this->smFactory->get($payment, PaymentTransitions::GRAPH);
        $sm->apply(PaymentTransitions::TRANSITION_COMPLETE);

        $this->payum->getHttpRequestVerifier()->invalidate($notify->getToken());

        throw new HttpResponse('OK', Response::HTTP_OK);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request): bool
    {
        /** @var PaymentInterface $payment */

        return $request instanceof Notify
            && $request->getModel() instanceof PaymentInterface;
    }
}
