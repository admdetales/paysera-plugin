<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Action;

use Omni\Sylius\PayseraPlugin\Model\Request;
use Omni\Sylius\PayseraPlugin\Payum\Request\GetRequest;
use Payum\Core\Action\ActionInterface;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\GetHttpRequest;

/**
 * Class GetRequestAction.
 */
final class GetRequestAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function execute($request): void
    {
        /** @var GetRequest $request */
        $httpRequest = new GetHttpRequest();
        $this->gateway->execute($httpRequest);

        if (false === isset($httpRequest->query['data'], $httpRequest->query['ss1'], $httpRequest->query['ss2'])) {
            return;
        }

        $request->result = new Request(
            $httpRequest->query['data'],
            $httpRequest->query['ss1'],
            $httpRequest->query['ss2']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request): bool
    {
        return $request instanceof GetRequest;
    }
}
