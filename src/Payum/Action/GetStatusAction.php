<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Action;

use Omni\Sylius\PayseraPlugin\Payum\Request\GetRequest;
use Omni\Sylius\PayseraPlugin\Payum\Request\ValidateRequest;
use Omnipay\Paysera\Gateway;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Sylius\Bundle\PayumBundle\Request\GetStatus;
use Sylius\Component\Core\Model\PaymentInterface;

/**
 * Class GetStatusAction.
 *
 * @property Gateway $api
 */
final class GetStatusAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;

    /**
     * @var bool
     */
    private $useOrderNumber = false;

    /**
     * @param bool $useOrderNumber
     */
    public function __construct($useOrderNumber = false)
    {
        $this->useOrderNumber = $useOrderNumber;
        $this->apiClass = Gateway::class;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($status): void
    {
        /** @var GetStatus $status */
        $request = new GetRequest();
        $this->gateway->execute($request);
        if (null === $request->result) {
            $status->markNew();

            return;
        }

        /** @var PaymentInterface $payment */
        $payment = $status->getModel();
        $valid = new ValidateRequest(
            $request->result,
            $this->api->getPassword(),
            $this->useOrderNumber ? $payment->getOrder()->getNumber() : (string)$payment->getOrder()->getId()
        );
        $this->gateway->execute($valid);

        if (false === $valid->isValid()) {
            throw new HttpResponse('', 400);
        }

        $status->markPending();
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request): bool
    {
        return $request instanceof GetStatus
            && $request->getModel() instanceof PaymentInterface
            && null !== $request->getModel()->getOrder();
    }
}
