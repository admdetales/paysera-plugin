<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Action;

use Omni\Sylius\PayseraPlugin\Payum\Paysera;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Request\Convert;
use Sylius\Component\Core\Context\ShopperContextInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class ConvertPaymentPayseraAction implements ActionInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var ShopperContextInterface
     */
    private $context;

    /**
     * @var bool
     */
    private $useOrderNumber = false;

    /**
     * ConvertPaymentPayseraAction constructor.
     *
     * @param UrlGeneratorInterface $router
     * @param ShopperContextInterface $context
     * @param bool $useOrderNumber
     */
    public function __construct(UrlGeneratorInterface $router, ShopperContextInterface $context, bool $useOrderNumber)
    {
        $this->router = $router;
        $this->context = $context;
        $this->useOrderNumber = $useOrderNumber;
    }

    /**
     * {@inheritdoc}
     *
     * @param Convert $request
     */
    public function execute($request): void
    {
        /** @var PaymentInterface $payment */
        $payment = $request->getSource();
        /** @var OrderInterface $order */
        $order = $payment->getOrder();
        $customer = $order->getCustomer();
        $shipping = $order->getShippingAddress();

        $request->setResult(
            [
                'transactionId' => $this->useOrderNumber ? $order->getNumber() : (string)$order->getId(),
                'returnUrl' => $request->getToken()->getAfterUrl(),
                'cancelUrl' => $this->router->generate(
                    'sylius_shop_account_order_index',
                    [
                        '_locale' => $this->context->getLocaleCode(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'language' => Paysera::LANGUAGE_LIT,
                'currency' => Paysera::CURRENCY_EUR,
                'amount' => abs($payment->getAmount() / 100),
                'card' => [
                    'firstName' => $customer->getFirstName(),
                    'lastName' => $customer->getLastName(),
                    'email' => $customer->getEmail(),
                    'city' => $shipping->getCity(),
                    'address1' => $shipping->getStreet(),
                    'postCode' => $shipping->getPostcode(),
                    'country' => $shipping->getCountryCode(),
                    'state' => $shipping->getProvinceName(),
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request): bool
    {
        return $request instanceof Convert
            && $request->getSource() instanceof PaymentInterface
            && $request->getTo() === 'array';
    }
}
